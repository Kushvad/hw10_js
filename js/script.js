const tabsBtn = document.querySelectorAll('.tabs-title');
const tabsItem = document.querySelectorAll('.tabs-item')

tabsBtn.forEach(function(item){
    item.addEventListener('click', function() {
        let tabClass = item.getAttribute('data-tab');
        let currentTab = document.querySelector(tabClass);
       
        tabsBtn.forEach(function(item) {
            item.classList.remove('active');
        });

        tabsItem.forEach(function(item) {
            item.classList.remove('active')
        });
        item.classList.add('active');
        currentTab.classList.add('active');
    });
});

